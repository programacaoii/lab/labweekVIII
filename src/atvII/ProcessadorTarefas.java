package atvII;

import java.util.List;
import java.util.Scanner;

public class ProcessadorTarefas {

	public void processarTarefas(RepositorioTarefa repositorio) {

		Scanner scanner = new Scanner(System.in);
		List<Tarefa> tarefas = repositorio.listarTarefas();
		int opcao;

		do {
			System.out.println("\nMenu:");
			System.out.println("1 - Adicionar Tarefa");
			System.out.println("2 - Verificar Tarefas");
			System.out.println("3 - Excluir Tarefa");
			System.out.println("4 - Atualizar Tarefa");
			System.out.println("5 - Marcar Como concluída");
			System.out.println("6 - Listar Tarefas Concluídas");
			System.out.println("7 - Listar Tarefas Pendentes");
			System.out.println("0 - Sair");
			System.out.print("Escolha uma opção: ");

			opcao = scanner.nextInt();
			scanner.nextLine();

			switch (opcao) {
			case 1:

				System.out.print("Digite o título da tarefa: ");
				String titulo = scanner.nextLine();
				System.out.print("Digite a descrição da tarefa: ");
				String descricao = scanner.nextLine();

				Tarefa novaTarefa = new Tarefa(titulo, descricao);
				if(repositorio.adicionarTarefa(novaTarefa)){
					System.out.println("Adicionado com sucesso!");
				}else {
					System.out.println("Tarefa com o título repetido não pode ser adicionada. ");
				}
				break;

			case 2:
				tarefas = repositorio.listarTarefas();
				if (tarefas.isEmpty()) {
					System.out.println("Nenhuma tarefa encontrada.");
				} else {
					System.out.println("\nTarefas:");
					for (Tarefa tarefa : tarefas) {
						System.out.println(tarefa);
					}
				}
				break;

			case 3:

				System.out.print("Digite o título da tarefa a ser excluída: ");
				String tituloExcluir = scanner.nextLine();
				Tarefa tarefaExcluir = repositorio.buscarTarefa(tituloExcluir);

				if (tarefaExcluir != null) {
					repositorio.removerTarefa(tarefaExcluir);
					System.out.println("Tarefa excluída com sucesso.");
				} else {
					System.out.println("Tarefa não encontrada.");
				}
				break;
			case 4:

				System.out.print("Digite o título da tarefa a ser atualizada: ");
				String tituloAtualizar = scanner.nextLine();
				Tarefa tarefaAtualizar = repositorio.buscarTarefa(tituloAtualizar);

				if (tarefaAtualizar != null) {
					System.out.print("Digite a nova descrição da tarefa: ");
					String novaDescricao = scanner.nextLine();
					tarefaAtualizar.setDescricao(novaDescricao);
					repositorio.atualizarTarefa(tarefaAtualizar);
					System.out.println("Tarefa atualizada com sucesso.");
				} else {
					System.out.println("Tarefa não encontrada.");
				}
				break;

			case 5:

				System.out.print("Digite o título da tarefa a ser marcada como concluída: ");
				String tituloConcluir = scanner.nextLine();
				Tarefa tarefaConcluir = repositorio.buscarTarefa(tituloConcluir);

				if (tarefaConcluir != null) {
					tarefaConcluir.marcarComoConcluida();
					repositorio.atualizarTarefa(tarefaConcluir);
					System.out.println("Tarefa marcada como concluída.");
				} else {
					System.out.println("Tarefa não encontrada.");
				}
				break;

			case 6:

				List<Tarefa> concluidas = ((RepositorioTarefasMemoria) repositorio).listarTarefasConcluidas();
				if (concluidas.isEmpty()) {
					System.out.println("Nenhuma tarefa concluída encontrada.");
				} else {
					System.out.println("\nTarefas Concluídas:");
					for (Tarefa tarefa : concluidas) {
						System.out.println(tarefa);
					}
				}
				break;

			case 7:

				List<Tarefa> pendentes = ((RepositorioTarefasMemoria) repositorio).listarTarefasPendentes();
				if (pendentes.isEmpty()) {
					System.out.println("Nenhuma tarefa pendente encontrada.");
				} else {
					System.out.println("\nTarefas Pendentes:");
					for (Tarefa tarefa : pendentes) {
						System.out.println(tarefa);
					}
				}
				break;

			case 0:
				System.out.println("Saindo...");
				scanner.close();
				break;

			default:
				System.out.println("Opção inválida. Digite um número válido.");
			}
		} while (opcao != 0);
	}
}

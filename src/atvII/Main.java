package atvII;

public class Main {
	public static void main(String[] args) {
		RepositorioTarefa repositorioMemoria = new RepositorioTarefasMemoria();
		ProcessadorTarefas processador = new ProcessadorTarefas();
		processador.processarTarefas(repositorioMemoria);
	}
}

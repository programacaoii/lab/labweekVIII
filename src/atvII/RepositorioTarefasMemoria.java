package atvII;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class RepositorioTarefasMemoria implements RepositorioTarefa {
	private List<Tarefa> tarefas = new ArrayList<>();

	@Override
	 public boolean adicionarTarefa(Tarefa tarefa) {
        for (Tarefa existe : tarefas) {
            if (existe.getTitulo().equals(tarefa.getTitulo())) {
                return false;
            }
        }
        return tarefas.add(tarefa);
    }

	@Override
	public void removerTarefa(Tarefa tarefa) {
		Iterator<Tarefa> iterator = tarefas.iterator();
		while (iterator.hasNext()) {
			Tarefa storedTarefa = iterator.next();
			if (storedTarefa.getTitulo().equals(tarefa.getTitulo())) {
				iterator.remove();
				break;
			}
		}
	}

	@Override
	public void atualizarTarefa(Tarefa tarefa) {
		for (int i = 0; i < tarefas.size(); i++) {
			if (tarefas.get(i).getTitulo().equals(tarefa.getTitulo())) {
				tarefas.set(i, tarefa);
				break;
			}
		}
	}

	@Override
	public Tarefa buscarTarefa(String titulo) {
		for (Tarefa tarefa : tarefas) {
			if (tarefa.getTitulo().equals(titulo)) {
				return tarefa;
			}
		}
		return null;
	}

	@Override
	public List<Tarefa> listarTarefas() {
		return new ArrayList<>(tarefas);
	}

	public List<Tarefa> listarTarefasConcluidas() {
		List<Tarefa> concluidas = new ArrayList<>();
		for (Tarefa tarefa : tarefas) {
			if (tarefa.isConcluida()) {
				concluidas.add(tarefa);
			}
		}
		return concluidas;
	}

	public List<Tarefa> listarTarefasPendentes() {
		List<Tarefa> pendentes = new ArrayList<>();
		for (Tarefa tarefa : tarefas) {
			if (!tarefa.isConcluida()) {
				pendentes.add(tarefa);
			}
		}
		return pendentes;
	}
}
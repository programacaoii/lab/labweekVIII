package atvII;

import java.util.List;

public interface RepositorioTarefa {
    boolean adicionarTarefa(Tarefa tarefa);
    void atualizarTarefa(Tarefa tarefa);
    void removerTarefa(Tarefa tarefa);
    Tarefa buscarTarefa(String titulo);
    List<Tarefa> listarTarefas();
	
	
}

package atvI.DRY;

public class SaudacoesSemDRY {

	public void saudacaoManha() {
		System.out.println("Bom dia!");
	}

	public void saudacaoTarde() {
		System.out.println("Boa tarde!");
	}

	public void saudacaoNoite() {
		System.out.println("Boa noite!");
	}
}

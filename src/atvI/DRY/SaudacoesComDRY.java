package atvI.DRY;

public class SaudacoesComDRY {
	public static void saudacao(String periodo) {
		System.out.println("Boa " + periodo + "!");
	}

	public void saudacaoManha() {
		saudacao("manhã");
	}

	public  void saudacaoTarde() {
		saudacao("tarde");
	}

	public  void saudacaoNoite() {
		saudacao("noite");
	}
}

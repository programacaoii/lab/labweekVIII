package atvI.KISS;

public class CalcularMediaComKISS {

	public double calcularMedia(int[] numeros) {
		int soma = 0;
		for (int num : numeros) {
			soma += num;
		}
		return (double) soma / numeros.length;
	}
}

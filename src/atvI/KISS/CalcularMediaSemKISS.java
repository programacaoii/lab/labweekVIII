package atvI.KISS;

public class CalcularMediaSemKISS {
	public double calcularMedia(int[] numeros) {
		int soma = 0;
		for (int i = 0; i < numeros.length; i++) {
			soma += numeros[i];
		}
		double media = soma / numeros.length;
		return media;
	}

}

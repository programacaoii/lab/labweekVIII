package atvI.KISS;

public class Main {
	public static void main(String[] args) {
		// Exemplo KISS
		CalcularMediaComKISS calculadora = new CalcularMediaComKISS();
		int[] numeros = { 5, 10, 15, 20 };
		double mediaSimples = calculadora.calcularMedia(numeros);
		System.out.println("Média com KISS: " + mediaSimples);

		// Exemplo sem KISS
		CalcularMediaSemKISS calculadoraSemKISS = new CalcularMediaSemKISS();
		double mediaSemKISS = calculadoraSemKISS.calcularMedia(numeros);
		System.out.println("Média Sem KISS: " + mediaSemKISS);
	}
}

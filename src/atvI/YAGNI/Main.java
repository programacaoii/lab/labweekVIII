package atvI.YAGNI;

public class Main {
	public static void main(String[] args) {
		CalculadoraComYAGNI calculadora = new CalculadoraComYAGNI();
		double resultadoSoma = calculadora.somar(5, 3);
		double resultadoMultiplicacao = calculadora.multiplicar(4, 6);
		double resultadoFatorial = calculadora.calcularFatorial(5);

		System.out.println("Resultado da soma: " + resultadoSoma);
		System.out.println("Resultado da multiplicação: " + resultadoMultiplicacao);
		System.out.println("Resultado do fatorial: " + resultadoFatorial);
	}
}

package atvI.YAGNI;

public class CalculadoraComYAGNI {
	public double somar(double a, double b) {
		return a + b;
	}

	public double multiplicar(double a, double b) {
		return a * b;
	}

	public double calcularFatorial(double n) {
		double resultado = 1;
		for (double i = 1; i <= n; i++) {
			resultado *= i;
		}
		return resultado;
	}
}
